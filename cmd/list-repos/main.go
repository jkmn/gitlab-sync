package main

import (
	"go/build"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/jkmn/errors"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/protocol/packp/sideband"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

func main() {
	token, ok := os.LookupEnv("GITLAB_TOKEN")
	if !ok {
		log.Fatal("Please set $GITLAB_TOKEN.")
	}
	cl := gitlab.NewClient(nil, token)

	projects, err := getProjects(cl)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	auth, err := ssh.NewPublicKeysFromFile("git", "/home/jackman/.ssh/id_rsa", "")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	gopath := build.Default.GOPATH
	gitlabPath := filepath.Join(gopath, "src", "gitlab.com")
	for _, project := range projects {
		// Does the directory exist?

		namespaceChunks := strings.Split(project.PathWithNamespace, "/")
		namespace := filepath.Join(namespaceChunks...)
		projectPath := filepath.Join(gitlabPath, namespace)
		log.Print(projectPath)
		//log.Printf("%s:\t%s", project.Name, project.PathWithNamespace)

		var exists bool
		dir, err := os.Open(filepath.Join(projectPath, ".git"))
		if os.IsNotExist(err) {

		} else if err != nil {
			log.Fatal(errors.Stack(err))
		} else {
			exists = true
		}

		if exists {
			err = dir.Close()
			if err != nil {
				log.Fatal(errors.Stack(err))
			}
		}

		if exists {
			log.Print("updating")
			err = updateRepo(project, projectPath, auth)
		} else {
			log.Print("cloning")
			err = cloneRepo(project, projectPath, auth)
		}
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
	}
	//jlog.Log(projects)
}

var (
	t bool = true
	//f bool = false
)

func getProjects(cl *gitlab.Client) ([]*gitlab.Project, error) {

	var (
		nextPage int = 1
		projects []*gitlab.Project
	)

	for nextPage != 0 {
		options := &gitlab.ListProjectsOptions{
			Owned: &t,
			ListOptions: gitlab.ListOptions{
				Page: nextPage,
			},
		}

		newProjects, resp, err := cl.Projects.ListProjects(options)
		if err != nil {
			return nil, errors.Stack(err)
		}

		projects = append(projects, newProjects...)
		nextPage = resp.NextPage
	}

	return projects, nil
}

func updateRepo(project *gitlab.Project, path string, auth transport.AuthMethod) error {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return errors.Stack(err)
	}

	options := &git.FetchOptions{
		Auth: auth,
	}
	err = repo.Fetch(options)
	if err == git.NoErrAlreadyUpToDate {
		// do nothing
	} else if err != nil {
		return errors.Stack(err)
	}

	return nil
}

func cloneRepo(project *gitlab.Project, path string, auth transport.AuthMethod) error {

	err := os.MkdirAll(path, os.ModeDir|0700)
	if err != nil {
		return errors.Stack(err)
	}

	cloneOptions := &git.CloneOptions{
		URL:      project.SSHURLToRepo,
		Progress: sideband.Progress(os.Stderr),
		Auth:     auth,
	}
	repo, err := git.PlainClone(path, false, cloneOptions)
	if err != nil {
		return errors.Stack(err)
	}

	options := &git.FetchOptions{
		Auth: auth,
	}
	err = repo.Fetch(options)
	if err == git.NoErrAlreadyUpToDate {
	} else if err != nil {
		return errors.Stack(err)
	}

	return nil
}
