.PHONY: fmt

fmt:
	for d in $(shell go list -f {{.Dir}} ./...); do goimports -w $$d/*.go; done
